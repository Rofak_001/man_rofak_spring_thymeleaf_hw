package com.hrd.com.spring_homework_thymeleaf.controller;

import com.hrd.com.spring_homework_thymeleaf.model.Article;
import com.hrd.com.spring_homework_thymeleaf.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;
import java.util.List;

@Controller
public class ArticleController {
    private ArticleRepository articleRepository;

    @Autowired
    public void setArticleRepository(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @GetMapping("/admin/review-article")
    public String articleReview(ModelMap modelMap) {
        List<Article> articleList=articleRepository.findAll();
        modelMap.addAttribute("articles",articleList);
        return "/admin/list-article";
    }

    @GetMapping("/admin/article")
    public String article(@ModelAttribute Article article, ModelMap modelMap) {
        modelMap.addAttribute("article", article);
        return "/admin/add-article";
    }

    @PostMapping("/admin/article")
    public RedirectView addArticle(@ModelAttribute @Valid Article article, BindingResult result) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(result.hasErrors()){
            return new RedirectView("/admin/article");
        }
        if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("admin"))) {
            articleRepository.save(article);
            return new RedirectView("/admin/review-article");
        }else{
            articleRepository.save(article);
            return new RedirectView("/admin/success");
        }
    }
    @GetMapping("/admin/success")
    public String saveSuccess(){
        return "/admin/success";
    }
}
