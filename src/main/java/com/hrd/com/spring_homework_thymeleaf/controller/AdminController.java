package com.hrd.com.spring_homework_thymeleaf.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.view.RedirectView;


@Controller
public class AdminController {
    @GetMapping("/admin/login")
    public String login() {
        return "login";
    }

    @GetMapping("/admin/dashboard")
    public String adminDashboard() {
        return "admin/dashboard";
    }

    @GetMapping("/admin/logout")
    public String adminLogout() {
        return "/admin/logout";
    }
    @GetMapping("/")
    public String home(){
        return "/admin/home";
    }
}
