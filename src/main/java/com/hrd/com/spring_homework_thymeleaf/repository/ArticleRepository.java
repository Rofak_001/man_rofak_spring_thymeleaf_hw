package com.hrd.com.spring_homework_thymeleaf.repository;

import com.hrd.com.spring_homework_thymeleaf.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends JpaRepository<Article,Integer> {
}
