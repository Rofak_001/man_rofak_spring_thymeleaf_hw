package com.hrd.com.spring_homework_thymeleaf.configuration;

import com.hrd.com.spring_homework_thymeleaf.custom.MySimpleUrlAuthenticationSuccessHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("dara").password("{noop}dara123").roles("USER").authorities("user")
                .and()
                .withUser("kanha").password("{noop}kanha123").roles("USER").authorities("editor")
                .and()
                .withUser("reksmey").password("{noop}reksmey123").roles("USER").authorities("review")
                .and()
                .withUser("makara").password("{noop}makara123").roles("ADMIN").authorities("admin","review","editor");
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/admin/dashboard").hasAuthority("admin")
                .antMatchers("/admin/article").hasAuthority("editor")
                .antMatchers("/admin/review-article").hasAuthority("review")
                .antMatchers("/admin/success").hasAuthority("editor")
                .antMatchers("/admin/logout","/403","/404","/500").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin().loginPage("/admin/login").loginProcessingUrl("/login").successHandler(myAuthenticationSuccessHandler()).permitAll()
                .and().logout().logoutUrl("/logout");


    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/css/**","/js/**","/img/**","/webfonts/**");
    }
    @Bean
    public AuthenticationSuccessHandler myAuthenticationSuccessHandler(){
        return new MySimpleUrlAuthenticationSuccessHandler();
    }
}
