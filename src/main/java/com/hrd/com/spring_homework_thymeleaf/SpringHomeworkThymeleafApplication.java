package com.hrd.com.spring_homework_thymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringHomeworkThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringHomeworkThymeleafApplication.class, args);
    }

}
